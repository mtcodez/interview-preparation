# Overlapping Subproblems

# memoization
# fibonacci

def fib_recursive(n)
	if n == 0 || n == 1
		return n
	end

	puts "computing fib_recursive(#{n})"
	return fib_recursive(n - 1) + fib_recursive(n - 2)
end

# The recursive version yields too many replications.
# Let's try an iterative version.

class Fib
	def initialize
		@memo = {}
	end

	def fib(n)
		if n < 0
			raise Exception, "Negative input."
		elsif n == 0 || n == 1
			return n
		end

		if @memo.include? n
			return @memo[n]
		end

		res = self.fib(n - 1) + self.fib(n - 2)
		@memo[n] = res
		return res
	end
end